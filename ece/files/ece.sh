. /usr/share/libubox/jshn.sh

ece_load() {
        local _data="$(/sbin/ece get "$1" 2>/dev/null)"
        [ "$_data" ] || return 1
        json_load "$_data"
        return 0
}
