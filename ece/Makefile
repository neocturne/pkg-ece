include $(TOPDIR)/rules.mk

PKG_NAME:=ece
PKG_VERSION:=2016-08-09
PKG_RELEASE=$(PKG_SOURCE_VERSION)

PKG_MAINTAINER:=Matthias Schiffer <mschiffer@universe-factory.net>

PKG_SOURCE_PROTO:=git
PKG_SOURCE_URL=https://gitlab.com/neoraider/ece.git
PKG_SOURCE_SUBDIR:=$(PKG_NAME)-$(PKG_VERSION)
PKG_SOURCE_VERSION:=a35d1e7174f7d9996601994b90ef1425584efacd
PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION)-$(PKG_SOURCE_VERSION).tar.gz
PKG_MIRROR_MD5SUM:=

PKG_LICENSE:=BSD-2-Clause
PKG_LICENSE_FILES:=COPYING

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/cmake.mk

define Package/eced
  SECTION:=base
  CATEGORY:=Base system
  DEPENDS:=+libubox +libubus
  TITLE:=Experimental Configuration Environment (daemon)
endef

define Package/libece
  SECTION:=libs
  CATEGORY:=Libraries
  DEPENDS:=+libubox +libubus +eced
  TITLE:=Experimental Configuration Environment (client library)
endef

define Package/lua-ece
  SUBMENU:=Lua
  SECTION:=lang
  CATEGORY:=Languages
  DEPENDS:=+libubus-lua +eced
  TITLE:=Experimental Configuration Environment (Lua client library)
endef

define Package/ece
  SECTION:=base
  CATEGORY:=Base system
  DEPENDS:=+libubox +libubus +libblobmsg-json +libjson-c +libece
  TITLE:=Experimental Configuration Environment (CLI client)
endef

CMAKE_OPTIONS += \
	-DCMAKE_BUILD_TYPE:STRING=MINSIZEREL \
	-DLUAPATH:PATH=lib/lua

define Package/ece/conffiles
/etc/config.ece
endef

define Package/eced/install
	$(INSTALL_DIR) $(1)/sbin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/sbin/eced $(1)/sbin/

	$(INSTALL_DIR) $(1)/etc
	$(INSTALL_CONF) ./files/config.ece $(1)/etc/config.ece

	$(INSTALL_DIR) $(1)/etc/init.d/
	$(INSTALL_BIN) ./files/eced.init $(1)/etc/init.d/eced

	$(INSTALL_DIR) $(1)/lib/ece/schema
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/doc/examples/system.schema.blob $(1)/lib/ece/schema/
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/doc/examples/wireless.schema.blob $(1)/lib/ece/schema/
endef

define Package/libece/install
	$(INSTALL_DIR) $(1)/lib
	$(INSTALL_DATA) $(PKG_INSTALL_DIR)/usr/lib/libece.so.0 $(1)/lib/
endef

define Package/lua-ece/install
	$(INSTALL_DIR) $(1)/usr/lib/lua
	$(INSTALL_DATA) $(PKG_INSTALL_DIR)/usr/lib/lua/ece.lua $(1)/usr/lib/lua/
endef

define Package/ece/install
	$(INSTALL_DIR) $(1)/sbin $(1)/lib/functions
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/sbin/ece $(1)/sbin/
	$(INSTALL_DATA) ./files/ece.sh $(1)/lib/functions/ece.sh
endef

$(eval $(call BuildPackage,eced))
$(eval $(call BuildPackage,libece))
$(eval $(call BuildPackage,lua-ece))
$(eval $(call BuildPackage,ece))
